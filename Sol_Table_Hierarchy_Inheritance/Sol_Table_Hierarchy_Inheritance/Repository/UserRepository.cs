﻿using Sol_Table_Hierarchy_Inheritance.EF;
using Sol_Table_Hierarchy_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Hierarchy_Inheritance.Repository
{
    public class UserRepository :PersonRepository
    {

        #region Constructor
        public UserRepository() : base()
        {

        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<UserEntity>> GetUserData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                       base.DbObject
                        ?.tbl_PersonAll
                        ?.OfType<User>()
                        ?.AsEnumerable()
                        ?.Select((leUserObj) => new UserEntity()
                        {
                            PersonId = leUserObj.PersonId,
                            FirstName = leUserObj.FirstName,
                            LastName = leUserObj.LastName,
                            MobileNo = leUserObj.MobileNo,
                            EmailId = leUserObj.EmailId,
                            UserName = leUserObj.UserName,
                            UPassword = leUserObj.UPassword
                        })
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
