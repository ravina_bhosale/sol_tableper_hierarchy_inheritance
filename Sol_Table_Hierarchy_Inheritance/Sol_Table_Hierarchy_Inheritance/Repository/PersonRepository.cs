﻿
using Sol_Table_Hierarchy_Inheritance.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Hierarchy_Inheritance.Repository
{
   public class PersonRepository
    {
        #region Declaration
        private PersonDBEntities db = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            db = new PersonDBEntities();

            this.DbObject = db;
        }
        #endregion

        #region Property
        protected  PersonDBEntities DbObject { get; set; }
        #endregion
    }
}
