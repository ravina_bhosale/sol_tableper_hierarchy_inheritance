﻿
using Sol_Table_Hierarchy_Inheritance.EF;
using Sol_Table_Hierarchy_Inheritance.Entity;
using Sol_Table_Hierarchy_Inheritance.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Hierarchy_Inheritance
{
   public class AdminRepository : PersonRepository
    {

        #region Constructor
        public AdminRepository() : base()
        {

        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<AdminEntity>> GetAdminData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                       base.DbObject
                        ?.tbl_PersonAll
                        ?.OfType<Admin>()
                        ?.AsEnumerable()
                        ?.Select((leAdminObj) => new AdminEntity()
                        {
                           PersonId=leAdminObj.PersonId,
                           FirstName=leAdminObj.FirstName,
                           LastName=leAdminObj.LastName,
                           MobileNo=leAdminObj.MobileNo,
                           EmailId=leAdminObj.EmailId,
                           AdminName=leAdminObj.AdminName,
                           APassword=leAdminObj.APassword
                        })
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

       
    }
}
