﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Hierarchy_Inheritance.Entity
{
    public class PersonEntity
    {
        public decimal PersonId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String MobileNo { get; set; }

        public String EmailId { get; set; }
    }
}
