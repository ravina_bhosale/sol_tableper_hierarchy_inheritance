﻿using Sol_Table_Hierarchy_Inheritance.Entity;
using Sol_Table_Hierarchy_Inheritance.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Table_Hierarchy_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<UserEntity> listUserObj =
                   await new UserRepository().GetUserData();

                IEnumerable<AdminEntity> listAdminObj =
                    await new AdminRepository().GetAdminData();
            

            }).Wait();

            
        }
    }
}
